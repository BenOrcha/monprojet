# MonProjet

POUR (public concerné par le produit)

QUI SOUHAITENT (formulation du besoin des cibles)

NOTRE PRODUIT EST (ce qu’est le produit)

QUI (le bénéfice majeur, l’utilité de la solution)

A LA DIFFERENCE DE (pratique actuelle, concurrence)

PERMET DE (éléments différentiateurs majeurs)